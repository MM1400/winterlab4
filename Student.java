public class Student {
	private String name;
	private double grade;
	private int id;
	int amountLearnt;
	public Student(String name, double grade) {
		this.amountLearnt = 0;
		this.name = name;
		this.grade = grade;
		this.id = 1234;
	}
	public void sayHi(String name) {
		System.out.println("hello " + name);
	}
	public void sayGrade(double grade) {
		System.out.println("Your grade is " + grade);
	}
	public void learn(int amountStudied) {
		if(amountStudied > 0) {
		this.amountLearnt = this.amountLearnt + amountStudied;
		}
	}
	public String getName() {
		return this.name;
	}	
	public double getGrade() {
		return this.grade;
	}
	public double getId() {
		return this.id;
	}	
	public void setId(int newId) {
		this.id = newId;
	}
}
